use nbt_fiddler::world::{World};

const WORLD_DIR: &'static str = "inputs/Akira_working";

fn fix_around_problems(world: &mut World) {
  for x in -1..2 {
    for y in -1..2i16 {
      for z in -1..2 {
        world.set_block_at(739 + x, (59 + y) as u8, -209 + z, "minecraft:air");
      }
    }
  }
}

fn find_problematic_chunks(world: &mut World, search_regions: &[(i32, i32)]) {
  for (x, z) in search_regions.iter() {
    let region = world.get_region(*x, *z).unwrap();
    let chunks = region.find_blocks("create:fluid_pipe");
    for chunk in chunks {
      println!(
        "Target block in region ({}, {}) in chunk ({}, {})", x, z, chunk.chunk_x, chunk.chunk_z);
    }
  }
}

fn replace_blocks(world: &mut World, search_regions: &[(i32, i32)], find: &str, replace: &str) {
  for (r_x, r_z) in search_regions.iter() {
    let region = world.get_region(*r_x, *r_z).unwrap();
    region.replace_blocks(find, replace);
  }
}

fn find_blocks(world: &mut World, search_regions: &[(i32, i32)], find: &str) {
  for (r_x, r_z) in search_regions.iter() {
    let region = world.get_region(*r_x, *r_z).unwrap();
    for chunk in region.find_blocks(find) {
      println!("  found block {} in chunk", find);
    }
  }
}

fn find_corrupt_blocks(world: &mut World, search_areas: &[((i32, i32), &[(u8, u8)])]) {
  for ((r_x, r_z), chunks) in search_areas.iter() {
    let region = world.get_region(*r_x, *r_z).unwrap();
    for (c_x, c_z) in chunks.iter() {
      let chunk = region.get_chunk(*c_x, *c_z).unwrap();
      chunk.print_corrupt_blocks();
    }
  }
}

fn remove_ticks(world: &mut World, search_areas: &[((i32, i32), &[(u8, u8)])]) {
  for ((r_x, r_z), chunks) in search_areas.iter() {
    let region = world.get_region(*r_x, *r_z).unwrap();
    for (c_x, c_z) in chunks.iter() {
      let chunk = region.get_chunk_mut(*c_x, *c_z).unwrap();
      chunk.remove_ticks();
    }
  }
}

fn remove_tile_entities(world: &mut World, search_areas: &[((i32, i32), &[(u8, u8)])], id: &str) {
  for ((r_x, r_z), chunks) in search_areas.iter() {
    let region = world.get_region(*r_x, *r_z).unwrap();
    for (c_x, c_z) in chunks.iter() {
      let chunk = region.get_chunk_mut(*c_x, *c_z).unwrap();
      chunk.remove_tile_entities(id);
    }
  }
}

fn fix_corrupt_blocks(world: &mut World, search_areas: &[((i32, i32), &[(u8, u8)])], with: &str) {
  for ((r_x, r_z), chunks) in search_areas.iter() {
    let region = world.get_region(*r_x, *r_z).unwrap();
    for (c_x, c_z) in chunks.iter() {
      let chunk = region.get_chunk_mut(*c_x, *c_z).unwrap();
      chunk.fix_corrupt_blocks(with);
    }
  }
}

pub fn main() {
  let mut world = World::new(WORLD_DIR);
  find_problematic_chunks(&mut world, &[(-1, 1), (-1, -1), (1, -1), (1, 1)]);
  // find_corrupt_blocks(&mut world, &[((1, -1), &[(14, 18), (13, 18)])]);
  // fix_corrupt_blocks(&mut world, &[((1, -1), &[(14, 18), (13, 18)])], "minecraft:air");
  // println!("Looking for fluid pipes and tanks");
  // find_blocks(&mut world, &[(1, -1)], "create:fluid_pipe");
  // println!("Replacing fluid pipes and tanks");
  // replace_blocks(&mut world, &[(1, -1)], "create:fluid_pipe", "minecraft:air");
  // println!("Looking for fluid pipes and tanks");
  // find_blocks(&mut world, &[(1, -1)], "create:fluid_pipe");
  remove_tile_entities(&mut world, &[((1, -1), &[(14, 18), (13, 18)])], "create:fluid_pipe");
  remove_ticks(&mut world, &[((1, -1), &[(14, 18), (13, 18)])]);
  world.save().unwrap();
}
