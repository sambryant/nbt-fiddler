pub mod nbt;
pub mod util;
pub mod world;
pub mod compression;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
