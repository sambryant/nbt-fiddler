mod packed_array;

pub use packed_array::*;

use std::fs;

pub fn scan(contents: &[u8], length: usize) {
  for i in 0..length {
    let byte: u8 = contents[i];
    println!("byte[{}] = {}", i, byte);
  }
}

pub fn scan_at(fname: &str, pos: usize, length: usize) {
  let contents = fs::read(fname).unwrap();
  for i in 0..length {
    println!("byte[{}] = {}", pos + i, contents[pos + i]);
  }
}

pub fn truncated_list_str<T: std::fmt::Display>(list: &[T], limit: usize) -> String {
  if list.len() <= limit {
    let list_label = list.iter().map(|v| format!("{}", v)).collect::<Vec<String>>().join(", ");
    format!("[{}]", list_label)
  } else {
    let first = &list[0..(limit - 2)];
    let list_label = first.iter().map(|v| format!("{}", v)).collect::<Vec<String>>().join(", ");
    format!("[{}, ..., {} ({} total)]", list_label, list[list.len() - 1], list.len())
  }
}

pub fn indent_str(indent: u8) -> &'static str {
  match indent {
    0 => "",
    1 => "  ",
    2 => "    ",
    3 => "      ",
    4 => "        ",
    5 => "          ",
    6 => "            ",
    7 => "              ",
    8 => "                ",
    _ => "...",
  }
}

pub fn binary_rep(byte: u8) -> String {
  let mut s = String::new();
  for i in 0..8 {
    if (byte & (1 << (8 - i - 1))) > 0 {
      s.push('1');
    } else {
      s.push('0');
    }
  }
  s
}

pub fn long_binary_rep(value: i64) -> String {
  let bit_mask = (1 << 8) - 1;
  (0..8).into_iter().map(|byte_index| {
    let shifted = value >> (8 * (8 - 1 - byte_index));
    binary_rep((shifted & bit_mask) as u8)
  })
  .collect::<Vec<String>>()
  .join(" ")
}

#[cfg(test)]
mod test_long_binary_rep {
  use super::long_binary_rep;

  #[test]
  fn it_can_work_basic() {
    let value: i64 = 1;
    let expected = "00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000001";
    assert_eq!(long_binary_rep(value), expected);
  }

  #[test]
  fn it_can_work_complicated() {
    let value = (3 << 56) + (2 << 48) + 17;
    let expected = "00000011 00000010 00000000 00000000 00000000 00000000 00000000 00010001";
    assert_eq!(long_binary_rep(value), expected);
  }
}
